Role Name
=========

Deploy Kafka Cluster with KRaft.

Role Variables
--------------

Checkout the details in `defaults/main.yml`

Example Inventory
-----------------

    [kafka_cluster]
    kafka0 ansible_host=192.168.56.21
    kafka1 ansible_host=192.168.56.22
    kafka2 ansible_host=192.168.56.23

Example Playbook
----------------

    - hosts: kafka_cluster
      roles:
         - kafka-kraft-cluster

How to install?
----------------
Ref. https://galaxy.ansible.com/docs/using/installing.html AND https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html

License
-------

MIT

Author Information
------------------

haibin.lee@foxmail.com
